﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace agua_software.mostrar
{
    public class mostrardato
    {

        private string _id_producto;
        private string _nombre_producto;
        private int _cantidad;
        private int _valor_unitario;

        public string Id_producto { get => _id_producto; set => _id_producto = value; }
        public string Nombre_producto { get => _nombre_producto; set => _nombre_producto = value; }
        public int Cantidad { get => _cantidad; set => _cantidad = value; }
        public int Valor_unitario { get => _valor_unitario; set => _valor_unitario = value; }
    }
}
