﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using entidadcla;
using domin;
using cladatos;
 using System.Data.SqlClient;

namespace agua_software
{
    public partial class Form1 : Form
    {
        

                public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]

        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
         
        
                ReleaseCapture();
                SendMessage(this.Handle, 0x112, 0xf012, 0);
            
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);

        }

        private void button1_Click(object sender, EventArgs e)
        {


            if (textBox1.Text != "")
            {
                if(textBox2.Text != "")
                {
                    modelo user = new modelo();

                    var validlogin = user.login(textBox1.Text, textBox2.Text);
                    if (validlogin == true)
                    {
                        this.Hide();
                        principal pr = new principal();
                        pr.FormClosed += logout;
                        bienvenido bw = new bienvenido();
                        bw.ShowDialog();
                 
                        pr.Show();
                     
                    }
                    else
                    {
                        msgerror("datos ingresados incorrectos");
                        textBox1.Clear();
                        textBox2.Clear();
                    }
                }
                else msgerror("ingrese su contraseña");

            }
            else msgerror("ingrese su usuario");


            
        }
        private void msgerror(string mesa)
        {
            label3.Text = mesa;
            label3.Visible = true;
        }

        private void logout(object sender, FormClosedEventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            label3.Visible = false;
            this.Show();
            textBox1.Focus();
        }
    }
}
