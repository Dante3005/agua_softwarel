﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using agua_software.capadatos;

namespace agua_software
{
    public partial class crearU : Form
    {
        int indice = 0;
        public crearU()
        {
            InitializeComponent();

       
        }
   
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
       
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]

        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

      



        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            indice = comboBox1.SelectedIndex;


        }

      
        private void button2_Click(object sender, EventArgs e)
        {



            BaseDeDatos bd = new BaseDeDatos();
            capadatos.cldata ins = new cldata();
            if (textBox2.Text.Length > 0 && textBox2.Text.Length < 10)
            {
                if (textBox3.Text.Length >= 5)
                {



                    try
                    {
                        ins.insertarUsua(textBox1.Text, textBox2.Text, textBox3.Text, comboBox1.Text);
                        MessageBox.Show("tu usuario ha sido creado ");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("usuario ya registrado");
                    }


                }
                else
                    MessageBox.Show("su contraseña debe tener mas de 5 caracteres");
            }
            else
                MessageBox.Show("su usuario debe tener menos de 10 caracteres ");
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";

        }

      

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

