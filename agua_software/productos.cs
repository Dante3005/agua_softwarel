﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using agua_software.capadatos;



namespace agua_software
{
    public partial class productos : Form
    {
        public productos()
        {
            InitializeComponent();
            dataGridView1.AllowUserToAddRows = false;
            consulta();
            
        }
        string operacion = "insertar";
        SqlConnection conexion1 = new SqlConnection("Data Source=192.168.1.6;Initial catalog=venta_agua;user id=usuario;password=dante3005 ");

        public void consulta()
        {
            string consul = "select *from productos";
            SqlCommand comando = new SqlCommand(consul, conexion1);
            SqlDataAdapter data = new SqlDataAdapter(comando);
            DataTable tabla = new DataTable();
            data.Fill(tabla);
            dataGridView1.DataSource = tabla;
        }

            private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]

        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void productos_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        BaseDeDatos bd = new BaseDeDatos();
        private void button1_Click(object sender, EventArgs e)
        {
       
            capadatos.cldata ins = new cldata();

            try
            {
                if (operacion == "insertar")
                {

                    ins.insertar(textBox1.Text, textBox2.Text, Convert.ToInt16(maskedTextBox1.Text), Convert.ToInt16(maskedTextBox2.Text));
                    MessageBox.Show("dato insertado ");
                    consulta();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("dato ya registrado");

           }


        
           

                if (operacion == "editar")
                {

                    ins.editar(textBox1.Text, textBox2.Text, Convert.ToInt16(maskedTextBox1.Text), Convert.ToInt16(maskedTextBox2.Text));
                    MessageBox.Show("se edito correctamente");

           
                operacion = "insertar";
                consulta();
            }


            textBox1.Clear();
            textBox2.Clear();
            maskedTextBox1.Clear();
            maskedTextBox2.Clear();
            consulta();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                operacion = "editar";

                textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
              
                textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                maskedTextBox1.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                maskedTextBox2.Text= dataGridView1.CurrentRow.Cells[3].Value.ToString();
            }
            else
            {
                MessageBox.Show("debes de seleccionar una fila");
            }
            consulta();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            capadatos.cldata ins = new cldata();

            if (dataGridView1.SelectedRows.Count > 0)
            {
               ins.EliminarProducto(dataGridView1.CurrentRow.Cells[0].Value.ToString());
               
                MessageBox.Show("Se elimino satisfactoriamente");
                consulta();
                   
            }
            else
                MessageBox.Show("Seleccione una fila");
            consulta();
        }

        private void button2_Click(object sender, EventArgs e)
         
        {
            capadatos.cldata ins = new cldata();

            ins.buscar(textBox1.Text);
            textBox1.Clear();
            textBox2.Clear();
            maskedTextBox1.Clear();
            maskedTextBox2.Clear();



        }

        public void mostrar(string buscar)
        {
            capadatos.cldata ins = new cldata();
          
            dataGridView1.DataSource = ins.buscar(buscar);
        }
        private void productos_Load(object sender, EventArgs e)
        {
          
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            mostrar(textBox3.Text);
        }

  

     
    }

        }
