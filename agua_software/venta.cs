﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using agua_software.capadatos;
using System.Data.SqlClient;
using agua_software.vista;
using agua_software.capadatos;
using agua_software.mostrar;
using Microsoft.Reporting.WebForms;

namespace agua_software
{
    public partial class venta : Form {

        ReportDataSource rs = new ReportDataSource();
        private decimal totalpagado = 0;
  








        public venta()
        {
            InitializeComponent();
            dataGridView1.AllowUserToAddRows = false;

        }

        public venta(venta owner)
        {
            Owner = owner;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;

        }



        private void llenar()
        {




        }
        private void venta_Load(object sender, EventArgs e)
        {
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            textBox5.Enabled = false;
            label9.Text = "0.0 $";
            label13.Text = "0.0 $";
            timer1.Start();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            vista.Form2 form = new vista.Form2();
            AddOwnedForm(form);
            form.ShowDialog();
        }

        private void iconButton1_Click_1(object sender, EventArgs e)

        {
            decimal total = 0, dto = 0, d = 0;
            if (dataGridView1.SelectedRows.Count > 0)
            {
                dataGridView1.Rows.Remove(dataGridView1.CurrentRow);

                MessageBox.Show("Se elimino satisfactoriamente");

                obtenerTotal();
               
            }
            else
                MessageBox.Show("Seleccione una fila");

        }





        private void iconButton3_Click_1(object sender, EventArgs e)
        {
            if (this.idventas.Text == string.Empty || this.textBox1.Text == string.Empty || this.textBox4.Text == string.Empty)
            {

                MessageBox.Show("llene todos los datos");


            }
            else
            {


                decimal total = 0, dto = 0;

                DataGridViewRow fila = new DataGridViewRow();

                fila.CreateCells(dataGridView1);

                DateTime j = Convert.ToDateTime(dateTimePicker1.Value);
            
                fila.Cells[0].Value = textBox2.Text;
                fila.Cells[1].Value = textBox3.Text;
                fila.Cells[2].Value = textBox4.Text;
                fila.Cells[3].Value = textBox5.Text;
                fila.Cells[4].Value = j;
                fila.Cells[5].Value = (float.Parse(textBox5.Text) * float.Parse(textBox4.Text)).ToString();
                dataGridView1.Rows.Add(fila);


                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    total = Convert.ToInt32(row.Cells[2].Value) * Convert.ToInt32(row.Cells[3].Value);
                    dto = dto + total;
                    label9.Text = dto.ToString();


                }

            }
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {

       
            if (this.idventas.Text == string.Empty || this.textBox1.Text == string.Empty || this.textBox4.Text == string.Empty || this.textBox6.Text == string.Empty || this.textBox7.Text == string.Empty)
            {

                MessageBox.Show("llene todos los datos");


            }
            else
            {

               
                capadatos.cldata ins = new cldata();

                DateTime j = Convert.ToDateTime(dateTimePicker1.Value);

                ins.vender(textBox6.Text, idventas.Text, textBox1.Text, decimal.Parse(label9.Text),j);


                foreach (DataGridViewRow r in dataGridView1.Rows)
                {

                    ins.cantidad(Convert.ToString(r.Cells[0].Value), Convert.ToInt32(r.Cells[2].Value));
                }

                MessageBox.Show("venta realizada ");

               clasfactura.CreaTicket Ticket1 = new clasfactura.CreaTicket();

                Ticket1.TextoCentro("AGUA CASCADA DEL ANGEL "); //imprime una linea de descripcion
                Ticket1.TextoCentro("**********************************");

                Ticket1.TextoIzquierda("CLIENTE: " +textBox6.Text);
               
                Ticket1.TextoIzquierda("");
                Ticket1.TextoCentro("Factura de Venta"); //imprime una linea de descripcion
                Ticket1.TextoIzquierda("No Fac:" +textBox1.Text);
                Ticket1.TextoIzquierda("Fecha:" + DateTime.Now.ToShortDateString() + " Hora:" + DateTime.Now.ToShortTimeString());
                Ticket1.TextoIzquierda("Le Atendio: "+ entidadcla.cache.userlogin2.usuario);
                Ticket1.TextoIzquierda("");
              clasfactura.CreaTicket.LineasGuion();

             clasfactura.CreaTicket.EncabezadoVenta();
               clasfactura.CreaTicket.LineasGuion();
                foreach (DataGridViewRow r in dataGridView1.Rows)
                {
                    Ticket1.AgregaArticulo(Convert.ToString( r.Cells[1].Value), Convert.ToInt32(r.Cells[3].Value), Convert.ToInt32(r.Cells[2].Value), Convert.ToDouble(r.Cells[5].Value));

                }
               clasfactura.CreaTicket.LineasGuion();

                Ticket1.AgregaTotales("Efectivo a pagar", double.Parse(label9.Text));
                Ticket1.AgregaTotales("Efectivo Entregado:", double.Parse(textBox7.Text));
                Ticket1.AgregaTotales("Efectivo Devuelto:", double.Parse(label13.Text));

                // Ticket1.LineasTotales(); // imprime linea 

                Ticket1.TextoIzquierda(" ");
                Ticket1.TextoCentro("**********************************");
                Ticket1.TextoCentro("*     Gracias por preferirnos    *");

                Ticket1.TextoCentro("**********************************");
                Ticket1.TextoIzquierda(" ");

                string impresora = "Microsoft XPS Document Writer";
                Ticket1.ImprimirTiket(impresora);


                textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text =textBox5.Text=textBox6.Text=textBox7.Text= "";
              label9.Text = label13.Text  = "0";

             


            }
    }







        public void obtenerTotal()
        {
            float costot = 0;
            int contador = 0;

            contador = dataGridView1.RowCount;

            for (int i = 0; i < contador; i++)
            {
                costot += float.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
            }

          label9.Text = costot.ToString();
        }




        private void textBox7_TextChanged(object sender, EventArgs e)
        {

            try
            {
                label13.Text = (decimal.Parse(textBox7.Text) - decimal.Parse(label9.Text)).ToString();


            }
            catch { }

            if (textBox7.Text == "")
            {
                label13.Text = "";
            }

           


        }
         
      
        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

         
        }
    }
            
            }
    

