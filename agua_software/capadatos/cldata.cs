﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using agua_software.mostrar;

namespace agua_software.capadatos
{
   public class cldata
    {
        private conexion co = new conexion();
        private SqlCommand cmd = new SqlCommand();
        private SqlDataReader leer;
        SqlDataAdapter da;
        
        public DataTable lista()
        {
            DataTable tabla = new DataTable();
            cmd.Connection = co.open();
            cmd.CommandText = "select  * from productos";
            leer = cmd.ExecuteReader();
            leer.Close();
            co.cerrar();
            return tabla;
        }
        public DataTable crearu()
        {
            DataTable usu = new DataTable();
            cmd.Connection = co.open();
            cmd.CommandText = "select  * from productos";
            leer = cmd.ExecuteReader();
            leer.Close();
            co.cerrar();
            return usu;
        }

        public void insertarUsua(string nombre, string usuario, string contraseña, string id_area)
        {
            cmd.Connection = co.open();
            cmd.CommandText = "insertar124 ";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("nombre", nombre);
            cmd.Parameters.AddWithValue("@usuario", usuario);
            cmd.Parameters.AddWithValue("@contraseña", contraseña);
            cmd.Parameters.AddWithValue("@id_area", id_area);
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();


        }



        public void insertar(string id_producto, string nombre_producto, int cantidad, int valor_unitario)
        {
            cmd.Connection = co.open();
            cmd.CommandText = "insertar ";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id_producto", id_producto);
            cmd.Parameters.AddWithValue("@nombre_producto", nombre_producto);
            cmd.Parameters.AddWithValue("@cantidad", cantidad);
            cmd.Parameters.AddWithValue("@valor_unitario", valor_unitario);
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
        }
        //editar productos
        public void editar(string id_producto, string nombre_producto, int cantidad, int valor_unitario)
        {
            cmd.Connection = co.open();
            cmd.CommandText = "editar1 ";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@id_producto", id_producto);
            cmd.Parameters.AddWithValue("@nombre_producto", nombre_producto);
            cmd.Parameters.AddWithValue("@cantidad", cantidad);
            cmd.Parameters.AddWithValue("@valor_unitario", valor_unitario);
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
        }
        //eliminar productos
        public void EliminarProducto(string id_producto)
        {
            cmd.Connection = co.open();
            cmd.CommandText = "delete productos where id_producto='" + id_producto + "'";
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
            co.cerrar();





        }
        public List<mostrardato> buscar(string BUSCAR)
        {

            cmd.Connection = co.open();
            cmd.CommandText = ("buscar2");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BUSCAR", BUSCAR);
            leer = cmd.ExecuteReader();
          

            List<mostrar.mostrardato> mostrars = new List<mostrar.mostrardato>();
            while (leer.Read())
            {
                mostrars.Add(new mostrardato
                {
                    Id_producto = leer.GetString(0),
                    Nombre_producto = leer.GetString(1),
                    Cantidad = leer.GetInt32(2),
                    Valor_unitario = leer.GetInt32(3)





                }); 
                 
            }
            co.cerrar();
            leer.Close();
            return mostrars;



        }
        public DataTable pro()
        {
            DataTable tabla = new DataTable( );

            cmd.Connection = co.open();
            cmd.CommandText = "listapro ";
            cmd.CommandType = CommandType.StoredProcedure;
            leer = cmd.ExecuteReader();
            tabla.Load(leer);
            leer.Close();
            co.cerrar();
            return tabla;

        }
        public void vender(string nombre_cliente, string idventas, string numeroventas, Decimal valor_total, DateTime fecha)
        {
            cmd.Connection = co.open();
            cmd.CommandText = "inserventa01 ";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@nombre_cliente",nombre_cliente);
            cmd.Parameters.AddWithValue("@idventas",idventas);
            cmd.Parameters.AddWithValue("@numeroventas",numeroventas);
            cmd.Parameters.AddWithValue("@valor_total",valor_total);
            cmd.Parameters.AddWithValue("@fecha", fecha);
      

            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
        }
        public void cantidad (string id_producto, int cantidad)
        {
            cmd.Connection = co.open();
            cmd.CommandText = "actua ";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@codigo", id_producto);
            cmd.Parameters.AddWithValue("@cantidad", cantidad);
            cmd.ExecuteNonQuery();
            cmd.Parameters.Clear();
        }



    }

        }

