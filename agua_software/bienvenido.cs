﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using cladatos;
using entidadcla.cache;

using System.Data.SqlClient;

namespace agua_software
{
    public partial class bienvenido : Form
    {
        public bienvenido()
        {
            InitializeComponent();
        }

        int cont = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            if(this.Opacity<1) this.Opacity += 0.05;
            ProgressBar1.Value += 1;
            ProgressBar1.Text = ProgressBar1.Value.ToString();
            if (ProgressBar1.Value==100)
            {
                timer1.Stop();
                timer2.Start();
            }
        
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            this.Opacity -= 0.1;
            if (this.Opacity == 0)
            {
                timer2.Stop();
                this.Close();
            }
        }

        private void load()
        {
            label3.Text = entidadcla.cache.userlogin2.usuario;
        
        }


        private void bienvenido_Load(object sender, EventArgs e)
        {
            load();

                timer1.Start();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
