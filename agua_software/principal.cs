﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using cladatos;
using entidadcla.cache;
using entidadcla;

namespace agua_software
{
    public partial class principal : Form
    {
        private principal currentHijo;
        public principal()
        {
            InitializeComponent();
            timer1.Start();
            

           

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("esta seguro que desea finalizar sesion?", "warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) ;

            this.Close();
         
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]

        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {

            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void principal_Load(object sender, EventArgs e)
        {
          
            
            permisos();


            label2
                .Text = entidadcla.cache.userlogin2.usuario;

        }

        public void permisos()
        {
            if (userlogin2.rango == cargos.admin )
            {
                

            }
            else
            {
                iconButton1.Enabled = false;
                iconButton3.Enabled = false;

            }
        }
    


     
        private void abrirform (Form hijo)
    {
           if(currentHijo != null)
            {
                currentHijo.Close();
            }

           
            hijo.TopLevel = false;
            hijo.FormBorderStyle = FormBorderStyle.None;
            hijo.Dock = DockStyle.Fill;
            panel3.Controls.Add(hijo);
            panel3.Tag = hijo;
            hijo.BringToFront();
            hijo.Show();


    }



        private void iconButton1_Click(object sender, EventArgs e)
        {
            abrirform(new crearU());
        }

        private void iconButton3_Click(object sender, EventArgs e)
        {
            abrirform(new productos());


        }

        private void iconButton5_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("esta seguro que desea finalizar sesion?", "warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) ;

            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label4.Text = DateTime.Now.ToString("MM-dd-yyyy");
            label5.Text = DateTime.Now.ToString("HH-mm-ss");
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            abrirform(new venta());
        }

        private void iconButton4_Click(object sender, EventArgs e)
        {
            factura f = new factura();
            f.ShowDialog();
        }
    }
}
