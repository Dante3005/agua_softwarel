﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using entidadcla.cache;


namespace cladatos
{
   public class clda : Datos_
    {
        public bool login(string user, string pass)
        {
            using (var connec = GetConnection())
            {
                connec.Open();
                using (var command = new SqlCommand())
                {
                    command.Connection = connec;
                    command.CommandText = "select *from usuario where usuario= @usuario and contraseña=@contraseña";
                    command.Parameters.AddWithValue("@usuario", user);
                    command.Parameters.AddWithValue("@contraseña", pass);
                    command.CommandType = CommandType.Text;
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            userlogin2.usuario = reader.GetString(0);
                            userlogin2.clave = reader.GetString(2);
                            userlogin2.rango = reader.GetString(3);
                        }
                        return true;
                    }
                    else
                        return false;


                }
            }
        }
    }
}
